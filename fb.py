#!/usr/bin/env python2
""" 
	Regression prediction of stock price on N-th date by knowing N-1 series of earlier trades. 

	Download latest historical set from
	https://www.quandl.com/api/v3/datasets/WIKI/FB.csv
	run as python fb.py 

"""

__author__ = "Pavel Turbin"

import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets, cross_validation, metrics
from sklearn.linear_model import Ridge
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline
import csv
from datetime import datetime

N=5 # use previous 5 days to predict for the 6-th day

def main():
	# load stock data
	data = load_data()
	x_in, y_in, labels = data_to_features(data["train"])

	# estimate best degree and ridge 
	degree = find_best_degree(0.1, x_in, y_in)
	bestridge = find_best_ridge(degree, x_in, y_in)

	# train model using traning setn
	model = make_pipeline(PolynomialFeatures(degree, interaction_only=True), Ridge(bestridge))
	model.fit( x_in, y_in)
	# validate error of traning set, should be low
	y_hat_in = model.predict( x_in )
	print "train data MSE", metrics.mean_squared_error(y_hat_in, y_in)

	# load cross validation of predict set
	x_actual, y_actual, labels = data_to_features(data["predict"])

	y_predicted = model.predict( x_actual )
	print "predicted data MSE",  metrics.mean_squared_error(y_actual, y_predicted)

	# plot the result	
	plot_in(y_actual, y_predicted, labels)


def load_data_file(filename):
	ret = []
	with open(filename+'.csv', 'rb') as csvfile:
		csvreader = csv.reader(csvfile, delimiter=',')
		next(csvreader, None)  # skip the headers
		for row in csvreader:
			ret.append({ "date" : row[0], "open" : float(row[1]), "close": float(row[4])})
	return ret

def load_data():
	data = load_data_file( "WIKI-FB" )

	ret = {}

	predictset = 40 # use latest data to validate model
	ret["train"] = data[predictset:]
	ret["train"] = ret["train"][::-1] # reversing from old to new

	ret["predict"] =data[0 : predictset] # predict set is latest data
	ret["predict"] =ret["predict"][::-1]

	return ret 

def data_to_features(data):
	x = []
	y = []
	labels = []
	samples=N # N samples to extract 

	for i in range(0, len(data)-samples, samples):
		features=[]
		for s in range(0, samples):
			features.append(data[i+s]["close"])

		x.append(features)
		y.append(data[i+samples]["close"])

		labels.append( data[i+samples]["date"] )
	
	return x, y, labels

def extimate_error(y, y_hat):
	return metrics.mean_squared_error(y, y_hat)

def find_best_ridge(degree, x_in, y_in):

	# split set: 20%  cross validation, 80% train n set	
	X_train, X_test, y_train, y_test = cross_validation.train_test_split(x_in, y_in,
	    test_size=0.1, random_state=49)

	bestridge = 0
	besterror = 1000000 # assume some large

	for i in range(1, 200):
		ridge = 0.1 * i # try different ridgest to find smallest error with fixed degree
		model = make_pipeline(PolynomialFeatures(1), Ridge(ridge))
		model.fit(  X_train, y_train)

		# cross validate model on test set
		y_hat_test = model.predict( X_test )

		err = extimate_error(y_test, y_hat_test)
		if err < besterror:
			besterror = err
			bestridge = ridge

	return bestridge

def find_best_degree(bestridge, x_in, y_in):

	# same as finding best ridge
	# split set: 20%  cross validation, 80% train n set	
	X_train, X_test, y_train, y_test = cross_validation.train_test_split(x_in, y_in,
	    test_size=0.2, random_state=49)
	bestdegree = 0
	besterror = 1000000
	for degree in range(1, 4):
		model = make_pipeline(PolynomialFeatures(degree), Ridge(1))
		model.fit(  X_train, y_train)

		y_hat_test = model.predict( X_test )

		err = extimate_error(y_test, y_hat_test)
		
		if err < besterror:
			besterror = err
			bestdegree = degree
	return bestdegree

def plot_in(y_actual, y_predicted, labels):

	dates = []
	for l in labels: 
		dates.append(datetime.strptime(l, '%Y-%m-%d'))

	fig, ax = plt.subplots()

	line_actual, =ax.plot_date( dates, y_actual, 'r-')
	line_predicted,=ax.plot_date( dates, y_predicted, 'go')

	ax.set_title("Facebook stock price prediction")
	plt.legend([line_actual, line_predicted], ['actual', 'predicted'], loc=2)

	plt.ylabel('Close price')

	fig.autofmt_xdate()
	plt.show()


if __name__ == "__main__":
    main()

