package com.snapme;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.content.BroadcastReceiver;
import android.support.v4.content.LocalBroadcastManager;

public class MainActivity extends Activity implements OnClickListener {
 
    final static String TAG = "MainActivity";

    Button mStartButton;
    Button mStopButton;
    TextView mStatusText;

    private BroadcastReceiver mStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
          // Extract data included in the Intent
          Log.i(TAG, "StatusReceiver");
          updateControlStatus(intent);
        }
    };
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mStatusText = (TextView) findViewById(R.id.statusText);
        mStartButton = (Button) findViewById(R.id.playbutton);
        mStopButton = (Button) findViewById(R.id.stopbutton);

        mStartButton.setOnClickListener(this);
        mStopButton.setOnClickListener(this);
        
        LocalBroadcastManager.getInstance(this).registerReceiver(mStatusReceiver, 
                            new IntentFilter(DataCollectService.ACTION_STATUS));
        startService(new Intent(DataCollectService.ACTION_QUERY_STATUS).setClass(this, DataCollectService.class));
    }
    
    @Override
    protected void onDestroy() {
      // Unregister since the activity is not visible
      LocalBroadcastManager.getInstance(this).unregisterReceiver(mStatusReceiver);
      super.onDestroy();
    } 

    public void onClick(View target) {
        if (target == mStartButton)
            startService(new Intent(DataCollectService.ACTION_START).setClass(this, DataCollectService.class));
        else if (target == mStopButton)
            startService(new Intent(DataCollectService.ACTION_STOP).setClass(this, DataCollectService.class));
   }
    
   void updateControlStatus(Intent intent){
       Bundle extras = intent.getExtras();
       if (extras != null) {
           Integer value = extras.getInt("status");
           mStartButton.setEnabled(false);
           mStopButton.setEnabled(false);
           
           if( value == DataCollectService.RUNNING_STATUS_STOPPED ){
               mStatusText.setText("Not logging data");
               mStartButton.setEnabled(true);
           }
           else if( value == DataCollectService.RUNNING_STATUS_RUNNING ){
               mStatusText.setText("Logging: "+extras.getString("file"));
               mStopButton.setEnabled(true);
           }
       }
   }
}
