// Background service to collect sensor data
// (C) Pavel Turbin

package com.snapme;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager; 

public class DataCollectService extends Service {

    final static String TAG = "DataCollectService";

    public static final String ACTION_QUERY_STATUS = "com.snapme.action.QUERY_STATUS";
    public static final String ACTION_START = "com.snapme.action.START";
    public static final String ACTION_STOP = "com.snapme.action.STOP";
    public static final String ACTION_STATUS = "com.snapme.action.STATUS";

    public static final String INTENT_PARAMETER = "parameter";
    public static final String PARAMETER_TIMER  = "com.snapme.action.TIMER";

    public static final int RUNNING_STATUS_STOPPED = 1;
    public static final int RUNNING_STATUS_RUNNING = 2;
    
    static final int POLLING_TIME  = 15 * 1000; 
    
    int mStatus = RUNNING_STATUS_STOPPED;
    
    private SensorManager mSensorManager;
    
    // allocated sensors
    HashMap<Sensor, SensorEventListener> mSensors = new HashMap<Sensor, SensorEventListener>();
    HashMap<Sensor, ArrayList<Float> > mSensorsData = new HashMap<Sensor, ArrayList<Float> >();

    String mWorkingDirRoot = null;
    String mCurrentLogPath = null;
    boolean mIsFirstTimeWrite=true;
    LocationListener mLocationListener = null;
    float mLastAcceleration = 0;
    float mLastX = 0;
    float mLastY = 0;
    float mLastZ = 0;
    long  mLastStepAnalyzeTime = 0;
    long  mLastLogWritingTime = 0;
    int   mSteps = 0;
    
    
    private WakeLock mWakeLock = null;
    
    
    @Override
    public void onCreate() {
        Log.i(TAG, "Creating service");
    }

    /**
     * Called when we receive an Intent. When we receive an intent sent to us via startService(),
     * this is the method that gets called. So here we react appropriately depending on the
     * Intent's action, which specifies what is being requested of us.
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        createSensorManager();
        createWakeLock();
        
        handleRequestIntent(intent);

        return Service.START_STICKY; // run all the time
    }

    void handleRequestIntent(Intent intent){

        if( intent == null ){
            Log.i(TAG, "service, recreated");
            processStartRequest();
            return;
        }
      
        Bundle bd = intent.getExtras();
        if (bd != null){
             Log.e(TAG, "Unknown intent");
        }  
        else{ // start/stop
            String action = intent.getAction();
            if (action.equals(ACTION_QUERY_STATUS)) processQueryStatus();
            else if (action.equals(ACTION_START)) processStartRequest();
            else if (action.equals(ACTION_STOP)) processStopRequest();
        }
    }
    
    void traceSensorEvent(Sensor sensor){
        String text = "";
        text += "sensor: ";
        text += sensor.getName();
        text += " values ";
        for( int value = 0; value < mSensorsData.get(sensor).size(); value ++ ){
            text +=  mSensorsData.get(sensor).get(value);
            text += ",";
        }
          
        Log.d(TAG, text );
    }
    
    void saveSensorData(SensorEvent event, int values){
// very verbose tracing draining batteries         
        Log.d(TAG, "collect sensor " + event.sensor.getName() + " vals " + values + " arr " + 
                mSensorsData.get(event.sensor).size() + " evtvals " + event.values.length);
        
        synchronized (DataCollectService.class){
            boolean isAverageCollected = isAverageCollected(event.sensor);
            
            for( int value = 0; value < Math.min(values, event.values.length); value ++ ){
                if( isAverageCollected )
                    mSensorsData.get(event.sensor).set(value+1, mSensorsData.get(event.sensor).get(value+1) + 
                                                                Math.abs(event.values[value]) ); 
                else
                    mSensorsData.get(event.sensor).set(value+1, event.values[value] ); 
            }
            
            mSensorsData.get(event.sensor).set(0, mSensorsData.get(event.sensor).get(0) + 1);
            traceSensorEvent(event.sensor);
        }
    }

    boolean isAverageCollected(Sensor sensor){
        if( sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION)
            return true;
        return false;
    }
    
    void addDataPlaceHolder(Sensor sensor, int values){

        mSensorsData.put(sensor, new ArrayList<Float>());
        for( int value = 0; value < values+1; value ++ ){
            mSensorsData.get(sensor).add(0.0f ); 
        }
    }
    
    void resetSensorData(){
        synchronized (DataCollectService.class){
            mSteps = 0;

            for (Sensor sensor : mSensorsData.keySet()) {
                for( int value = 0; value < mSensorsData.get(sensor).size(); value ++ ){
                    mSensorsData.get(sensor).set(value, 0.0f ); 
                }
            }
        }
    }
 
    String getSensorColName(int type){
        
        switch(type){
            case  Sensor.TYPE_GAME_ROTATION_VECTOR: return "gmrotvec";
            case  Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR: return "georotvec";
            case  Sensor.TYPE_ORIENTATION: return "orientation";
            case  Sensor.TYPE_MAGNETIC_FIELD: return "magintic_field";
            case  Sensor.TYPE_PROXIMITY: return "proximity";
            case  Sensor.TYPE_ACCELEROMETER: return "accelerometer";
            case  Sensor.TYPE_LINEAR_ACCELERATION: return "linear_acceleration";
            case  Sensor.TYPE_GRAVITY: return "gravity";
            case  Sensor.TYPE_GYROSCOPE: return "gyroscope";
            case  Sensor.TYPE_ROTATION_VECTOR: return "rotation_vector";
    
            case  Sensor.TYPE_AMBIENT_TEMPERATURE: return "temperature";
            case  Sensor.TYPE_PRESSURE: return "pressure";
            case  Sensor.TYPE_RELATIVE_HUMIDITY: return "humidity";
            case  Sensor.TYPE_STEP_COUNTER: return "step";
        }
        return "unknown"+type;
    }

    void registerSensorMoveAlayzer(){
      int type = Sensor.TYPE_ACCELEROMETER;
      Sensor sensor = mSensorManager.getDefaultSensor(type);
      
      int values = 1; 
      if( sensor  !=null ){
          SensorEventListener listener = new SensorEventListener() {
                public void onSensorChanged(SensorEvent event) {

                      Log.d(TAG, "collect sensor " + event.sensor.getName() + "v0=" + event.values[0] + "v1=" + event.values[1] + "v2=" + event.values[2] );
                  
                      long curTime = System.currentTimeMillis();
                      
                      if( (curTime - mLastLogWritingTime ) >= POLLING_TIME ){
                          mLastLogWritingTime = curTime;
                          writeLogEntry();
                          resetSensorData();
                      }
                      
                      // after quick calibration found that step how long lasts, skip more frequent calls
                      if( mLastStepAnalyzeTime > 0 && (curTime - mLastStepAnalyzeTime  ) < 400 )
                          return;
                      mLastStepAnalyzeTime = curTime;
                  
                      float deltax =Math.abs(mLastX-event.values[0]); 
                      float deltay =Math.abs(mLastY-event.values[1]); 
                      float deltaz =Math.abs(mLastZ-event.values[2]); 
                      
                      boolean isStep = false;
                      // consider step if significant move by Z-axe 
                      if( deltaz > 1.5 &&  deltax < 10.0 && deltay < 10.0 ){
                        isStep = true;
                      }

                      mLastX = event.values[0];
                      mLastY = event.values[1];
                      mLastZ = event.values[2];
                      
                      if( isStep ){
                          synchronized (DataCollectService.class){
                              mSensorsData.get(event.sensor).set(1, mSensorsData.get(event.sensor).get(1) + 1 );
                          }
                      }
                }
      
                public void onAccuracyChanged(Sensor sensor, int accuracy) {}
            };
            
            mSensors.put( sensor, listener);
            addDataPlaceHolder(sensor, values);
            
            Log.i(TAG, "registered " + getSensorColName(type) );
        }
        else
          Log.d(TAG, "failed to create " + getSensorColName(type) );
    }
      
    void createSensorManager(){
        if( mSensorManager == null){
            mSensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
            registerSensorMoveAlayzer();
        }
    }
    
    void createWakeLock(){
        if( mWakeLock == null){
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "Snapme");
        }
    }
    
    void startSensors(){

      mLastStepAnalyzeTime = System.currentTimeMillis();
      mLastLogWritingTime = System.currentTimeMillis();

      for (Sensor sensor :  mSensors.keySet()) {
          mSensorManager.registerListener(mSensors.get(sensor), sensor, SensorManager.SENSOR_DELAY_NORMAL);
      }
      Log.i(TAG, "sensors started");
  } 
    void endSensors(){
        for (Sensor sensor :  mSensors.keySet()) {
            mSensorManager.unregisterListener(mSensors.get(sensor));         
        }
        Log.i(TAG, "ended sensors");
    }         
    
    void processQueryStatus(){
        Intent resultIntent = new Intent(ACTION_STATUS);
        resultIntent.putExtra("status", mStatus);
        resultIntent.putExtra("file", mCurrentLogPath == null ? "" : mCurrentLogPath);
        
        LocalBroadcastManager.getInstance(this).sendBroadcast(resultIntent);
    }

    void processStartRequest() {
        Log.i(TAG, "got start");
        if( mStatus == RUNNING_STATUS_STOPPED ){
            initWorkDirRoot();
          
            if( !createLoggingFile() )
                return;
            
            mStatus = RUNNING_STATUS_RUNNING;
            mSteps = 0;
  
            mWakeLock.acquire();
            resetSensorData();                 
            startSensors();
            processQueryStatus();
        }
    }

    void processStopRequest() {
        Log.i(TAG, "got process stop");
        if( mStatus == RUNNING_STATUS_RUNNING ){
            mStatus = RUNNING_STATUS_STOPPED;
            
            synchronized (DataCollectService.class){
                Log.i(TAG, "ending timers");
                
                writeLogEntry();
                endSensors();
                
                if( mWakeLock.isHeld() )
                    mWakeLock.acquire();
                mWorkingDirRoot = null;
                mCurrentLogPath = null;
                processQueryStatus();
            }
        }
    }


    void writeLogEntry() {
      
        List<Sensor> sortedSensors = new ArrayList<Sensor>(mSensors.size());
        sortedSensors.addAll( mSensors.keySet());
        
        Collections.sort(sortedSensors, new Comparator<Sensor>(){
          public int compare(Sensor lhs, Sensor rhs) {
              return new Integer(lhs.getType()).compareTo(new Integer(lhs.getType()));
          }
        }); 

        
        try {
          
          FileOutputStream outStream = new FileOutputStream( new File(mCurrentLogPath), true);
 
          // write header 
          if( mIsFirstTimeWrite) {
            
              mIsFirstTimeWrite = false; 
              outStream.write("time".getBytes());
              outStream.write(",date".getBytes());
              
              for (Sensor sensor : sortedSensors) {
                  outStream.write(",".getBytes());
                  
                  int datavalues = mSensorsData.get(sensor).size() -1;
                  if( datavalues == 1 ){
                      outStream.write( getSensorColName(sensor.getType()).getBytes());
                  }
                  else if( datavalues == 3 || datavalues == 4 ){
                      outStream.write( (getSensorColName(sensor.getType())+"-x,").getBytes());
                      outStream.write( (getSensorColName(sensor.getType())+"-y,").getBytes());
                      outStream.write( (getSensorColName(sensor.getType())+"-z").getBytes());
                      
                      if( datavalues == 4 )
                          outStream.write( (","+getSensorColName(sensor.getType())+"-r").getBytes());
                  } 
              }
              outStream.write("\n".getBytes());
          }

          outStream.write( ("" + System.currentTimeMillis()).getBytes() );
          SimpleDateFormat sdf = new SimpleDateFormat("MM-dd HH:mm:ss.SSS");
          String currentDateandTime = sdf.format(new Date());
          outStream.write( ("," + currentDateandTime ).getBytes());

          
          
          for (Sensor sensor : sortedSensors) {

              boolean isAverageCollected = isAverageCollected(sensor);
              float numberOfItems = mSensorsData.get(sensor).get(0);
              
              for( int i=1; i < mSensorsData.get(sensor).size(); i ++ ){
                  String text = ",";

                  float data = mSensorsData.get(sensor).get(i);
                  if( isAverageCollected ){
                      if( numberOfItems >= 1.0f )
                        data /= numberOfItems;
                  }

                  text += data; 
                  outStream.write( (text).getBytes());
              }
          }
          outStream.write("\n".getBytes());
          outStream.close();
        } 
        catch (IOException e){
            Log.e(TAG, "exeption "+e.toString());
        }
    }
    void initWorkDirRoot(){
      
      File workingDirAsFile = getExternalCacheDir ();
      
      if (!workingDirAsFile.exists()) {
          workingDirAsFile.mkdirs();
      }
      mWorkingDirRoot = workingDirAsFile.getAbsolutePath();
    }
    
    boolean createLoggingFile() {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) 
        {
            if( mWorkingDirRoot == null || mWorkingDirRoot.length() == 0)
                return false;
            
            mCurrentLogPath = mWorkingDirRoot;
            mCurrentLogPath += "/";
            mCurrentLogPath += "log";
            mCurrentLogPath += ".csv";
             
            File oldLog = new File(mCurrentLogPath); 
            mIsFirstTimeWrite = !oldLog.exists() ;
            
            return true;
        }
        return false;
    }

    @Override
    public void onDestroy() {
        // Service is being killed
        Log.i(TAG, "destroy");
        processStopRequest();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
}
