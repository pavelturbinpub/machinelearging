[{
        "y" : { // data from Jawbone wearable device 
            "active_time" : 11,
            "time_completed" : 1457150580,
            "enddate" : "2016-03-05 06:03:00",
            "distance" : 14.0,
            "startdate" : "2016-03-05 06:02:00",
            "calories" : 0.891089320183,
            "steps" : 18, // number of steps performed
            "time" : 1457150520,
            "speed" : 1.0
        }, // data
        "features" : [{ // Android sensor samples collected during one minute interval 
                "meta" : {
                    "date" : "2016-03-05 06:02:11",
                    "time" : 1457150531698
                },
                "accelerometer" : 10.0 // accelerometer data normalized to steps
            }, 
            ....
            ]
            
// (C) Pavel Turbin