#!/usr/bin/env python2
""" 
Script runs machine learning to predict a person steps based on recorded Android sensor data
"""

__author__ = "Pavel Turbin"

import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets, cross_validation, metrics
from sklearn import svm
import csv
import json
from datetime import datetime
from datetime import timedelta

from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import MultinomialNB

# Model specifications with differnt kernels
MODEL_SPECS = [
    { "model" : "SVC", "C": 0.5, "kernel": "rbf"},
    { "model" : "SVC", "C": 1.0, "kernel": "rbf"},
    { "model" : "SVC", "C": 4.0, "kernel": "rbf"},
    { "model" : "SVC", "C": 10.0, "kernel": "rbf"},
    { "model" : "SVC", "C": 1.0, "kernel": "sigmoid"},
    { "model" : "SVC", "C": 2.0, "kernel": "sigmoid"},
    { "model" : "GaussianNB" },
    { "model" : "OneClassSVM"},
]


def dataToFeatures( trainarray ):
    x = []
    y = []
    labels = []

    for td in trainarray:

        if "accelerometer" in td["features"][0]:
            accelerometer = 0.0
            feature = []
            for f in td["features"]: 
                accelerometer = accelerometer + float(f["accelerometer"])

            if accelerometer < 0.1 : continue
    
            # drop essential errorniouzs entries, with  getting more data this not required
            rate = float(td["y"]["steps"])/accelerometer 
            if  (1.9 < rate and rate < 6.0) :
                feature.append(accelerometer)
                y.append( td["y"]["steps"] )        
                x.append(feature)
                labels.append( { "y": td["y"], "feature": feature } )

        else: # high bias dataset
            feature = []
            for f in td["features"]: 
                feature.append( float(f["linear_acceleration-x"]) )
                feature.append( float(f["linear_acceleration-y"]) )
                feature.append( float(f["linear_acceleration-z"]) )

            y.append( td["y"]["steps"] )        
            x.append(feature)
    
    return x, y, labels

def getTrainSet():
    return ["20160303.trainarray.json","20160304.trainarray.json","20160305.trainarray.json"]

def getTrainSetBias():
    return [ "bias/trainarray.json.20160229", "bias/trainarray.json.20160301"]

def main():

    # load array of dataset
    trainarray = []
    for fl in getTrainSet():
        f = open( "dataset/" + fl, "rb" ); 
        arr = json.loads(f.read() );
        f.close()
        trainarray = trainarray + arr
        
    x_in, y_in, labels = dataToFeatures(trainarray)

# uncomment the following line to plot data input   
#    plotInputData(labels)
#    return

    print "Train set size:", len(y_in)

# find best model for the input data
    best_model_spec = findBestModel( x_in, y_in )

# uncomment the line to plot train/test errors relations
#    plotErrorDependsOnDataSetSize(x_in, y_in, best_model_spec )

    predictAndPlot(x_in, y_in, best_model_spec )

def plotInputData(labels):

    dates = []
    steps = []
    sensors = []

    # take series of samples in few hours range
    start_date = datetime(2016,3,4,6,0,0)
    end_date = start_date + timedelta(hours=5)
    print start_date, end_date
    points = []    
    pt = 1
    for l in labels: 
        dt = datetime.strptime(l["y"]["startdate"], '%Y-%m-%d  %H:%M:%S')
        if start_date <= dt and dt <= end_date:

            points.append(pt)
            pt += 1
            steps.append(l["y"]["steps"])
            sensors.append(l["feature"][0])

    fig, ax = plt.subplots()

    line_actual, =ax.plot( points, steps, 'r')
    line_sensor,=ax.plot( points, sensors, 'g')

    ax.set_title("Steps and sensor data")
    plt.legend([line_actual, line_sensor], ['steps', 'sensor'], loc=3)

    plt.ylabel('Jawbone Steps/Android sensor')
    plt.xlabel('train data point')

    plt.show()


def plotErrorDependsOnDataSetSize(x_in, y_in, model_spec):

    error_test = []
    error_train = []
    test_size = []

    # increase train size and measure test error     
    size_from = 0.8
    size_to   = 0.95
    steps = 10

    step= (size_to - size_from)/float(steps)
    for i in range(steps, 1, -1):
        model = createModel(model_spec)

        size = size_from + (i-1) * step
        test_size.append(size)
        X_train, X_test, y_train, y_test = cross_validation.train_test_split(x_in, y_in,
	        test_size=1.0-size, random_state=49)

        model.fit(X_train, y_train) 

        y_train_hat = model.predict( X_train )
        train_error = metrics.mean_squared_error(y_train_hat, y_train)

        y_test_hat = model.predict( X_test )
        test_error = metrics.mean_squared_error(y_test_hat, y_test)

        error_test.append(test_error)
        error_train.append(train_error)

    fig, ax = plt.subplots()

    y_mean = [np.mean(error_test) for i in test_size ]
    plt.plot(test_size,y_mean,  'r--')
    ax.set_title("Test error of bias dataset")

    line_test_error, = plt.plot(test_size, error_test, 'ro')
    line_train_error, = plt.plot(test_size, error_train, 'g', linewidth=2)

    plt.legend([ line_test_error, line_train_error], ['Test error', 'Train error'], loc=6  )
    plt.ylabel('MSE')
    plt.xlabel('data size, %')

    plt.show()


def predictAndPlot(x_in, y_in, model_spec):

    # train model with set and plot predicted data using test set
 
    X_train, X_test, y_train, y_test = cross_validation.train_test_split(x_in, y_in,
        test_size=0.15, random_state=49)

    model = createModel(model_spec)
    model.fit(X_train, y_train) 
    y_train_hat = model.predict( X_train )
    print "train data MSE", metrics.mean_squared_error(y_train_hat, y_train)

    y_test_hat = model.predict( X_test )
    print "*test data MSE", metrics.mean_squared_error(y_test_hat, y_test)

    fig, ax = plt.subplots()

    line_predicted, =ax.plot(y_test_hat, 'ro')
    line_actual,=ax.plot( y_test, 'g')

    ax.set_title("Predicted vs actual steps")
    plt.legend([line_predicted, line_actual], ['predicted', 'actual'], loc=3)

    plt.ylabel('Steps')
    plt.xlabel('test data points')

    plt.show()


def createModel( spec ):
    if spec["model"] == "SVC":
        kernel = None
        if "kernel" in spec:
            kernel = spec["kernel"]
        C = 1.0
        if "C" in spec:
            C = spec["C"]
        degree = 3
        if "degree" in spec:
            degree = spec["degree"]
        return svm.SVC(C=C, kernel=kernel, degree=degree)
    if spec["model"] == "GaussianNB":
        return GaussianNB()
    if spec["model"] == "MultinomialNB":
        return MultinomialNB()
    if spec["model"] == "OneClassSVM":
        return svm.OneClassSVM()

    return None

def findBestModel(x_in, y_in):

    # try different models and find with the lowest test error
    X_train, X_test, y_train, y_test = cross_validation.train_test_split(x_in, y_in,
	    test_size=0.2, random_state=49)

    res = []
    for spec in MODEL_SPECS:
        model = createModel(spec)
        model.fit(X_train, y_train) 
        y_train_hat = model.predict( X_train )
        y_test_hat = model.predict( X_test )
        
        res.append({ "model" : spec, "msetest": metrics.mean_squared_error(y_test_hat, y_test), "msetrain": metrics.accuracy_score(y_train_hat, y_train) })
        
    # sort by lowest test error
    res = sorted(res, cmp=lambda x,y: cmp(x["msetest"], y["msetest"]) )
    print "best model", res[0]
    return res[0]["model"]
 
if __name__ == "__main__":
    main()

